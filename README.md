This automatically generates database model files for IOS Swift using the data structure of a specified table in mysql / PHPMyAdmin.

To use, just create a database in PHPMyAdmin (table names in plural form please),
Open
src/mysqlmodelgenerator/MysqlModelGenerator.java
and customize the following variables:

    static String database="database";
    static String hostname="localhost";
    static String username="root";
    static String password="password";

Then run the program. The generated files will appear in the folder 

/src/models

Copy them to your Swift project. Also copy the contents of the folder src/utils.