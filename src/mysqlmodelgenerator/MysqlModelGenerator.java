/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mysqlmodelgenerator;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import utils.MySqlDBHelper;
import utils.fileaccess.FileWriter;

/**
 *
 * @author jaspertomas
 */
public class MysqlModelGenerator {
    
    static String database="database";
    static String hostname="localhost";
    static String username="root";
    static String password="password";

    public static void main(String args[])
    {
        String url = "jdbc:mysql://"+hostname+":3306/"+database;

        MySqlDBHelper.init(url, username, password);            
        Connection conn=MySqlDBHelper.getInstance().getConnection();
        
        Statement st = null;
        ResultSet rs = null;
        ArrayList<String> tables=new ArrayList<String>();
        ArrayList<String> fields=new ArrayList<String>();
        ArrayList<String> fieldtypes=new ArrayList<String>();
        try { 
            st = conn.createStatement();
            rs = st.executeQuery("Show Tables");

            while (rs.next()) {
                tables.add(rs.getString(1));
            }
            for(String table:tables) {
                rs = st.executeQuery("SHOW COLUMNS FROM "+table);
                fields.clear();
                fieldtypes.clear();
                while (rs.next()) {
                    fields.add(rs.getString(1));
                    fieldtypes.add(rs.getString(2));
                }
                createModelFile(table,fields,fieldtypes);
                createModelsFile(table,fields,fieldtypes);
                
                //create code for DatabaseHelper.dump()
//                System.out.println("for item in "+toCamelCase(table)+".select(\"\"){println(\""+toCamelCase(table)+" \"+item.commaSeparatedFieldsAndValues(includeId: true));println();}");
  
            //create SyncInHelper's processUpdate() switch statement
            //includes only tables with string id's, as only those are syncable
            //sample output:
            //      case "Apps": Apps.syncIn(update,errbank: errbank);break;
//                if(!fieldtypes.get(0).contains("int"))
//                    System.out.println("      case \""+toCamelCase(singularize(table))+"\": "+toCamelCase(table)+".syncIn(update,errbank: errbank);break;");
            
            }
        } catch (SQLException ex) {
            //Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public static void createModelsFile(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        //write file to current directory
        File outputdir=new File("./src/models");
        outputdir.mkdir();
        FileWriter.write(outputdir.getPath()+"/"+toCamelCase(table)+".swift", createModelsFileContents(table,fields,fieldtypes));
    }
    public static String createModelsFileContents(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        int counter=0;
        
        String fieldsstring="";
        for(int i=0;i<fields.size();i++)
        {
            fieldsstring+="\n            "+(i==0?"":",")+"\""+fields.get(i) +"\"";
        }
        
        String fieldtypesstring="";
        for(int i=0;i<fieldtypes.size();i++)
        {
            fieldtypesstring+="\n            "+(i==0?"":",")+"\""+datatypeFor(fieldtypes.get(i))+"\"";
        }
        
        String idfield=fields.get(0);
        String idfieldtype=fieldtypes.get(0);
        String idfieldtypestringifier=stringifier(fieldtypes.get(0));
        String iddatatype=datatypeFor(fieldtypes.get(0));
        
        String gettersandsetters="";
        String datatype="";
        String field="";
        String fieldtype="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            datatype=datatypeFor(fieldtypes.get(i));
            gettersandsetters+=
"\n    public "+datatype+" get"+toCamelCase(field)+"() {"
+"\n            return "+field+";"
+"\n    }"
+"\n"
+"\n    public void set"+toCamelCase(field)+"("+datatype+" "+field+") {"
+"\n            this."+field+" = "+field+";"
+"\n    }"
+"\n";
        }
        
        String vardefinitions="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            datatype=datatypeFor(fieldtypes.get(i));
            vardefinitions+=
"\n    public "+datatype+" "+field+";";
        }
                        
        String constructorfields="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            constructorfields+=
"\n            "+field+"=rs."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\");";
        }
 
        String implodevaluesstring="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            fieldtype=fieldtypes.get(i);
            implodevaluesstring+=
"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
        }
        
//        String savestring="";
//        if(iddatatype.contentEquals("String"))
//            savestring="\n            if("+idfield+"==null || "+idfield+".isEmpty() )";
//        else
//            savestring="\n            if("+idfield+"==null || "+idfield+"==0)";
        
        String testdata1="";
        if(fieldtypes.get(1).contains("varchar"))testdata1="\n        item."+fields.get(1)+"=\"ab\"";
        else if(fieldtypes.get(1).contains("int"))testdata1="\n        item."+fields.get(1)+"=1";
        String testdata2="";
        if(fieldtypes.get(1).contains("varchar"))testdata2="\n        item."+fields.get(1)+"=\"cd\"";
        else if(fieldtypes.get(1).contains("int"))testdata2="\n        item."+fields.get(1)+"=2";
        
        String hasstringid=(fieldtypes.get(0).contains("varchar")?"true":"false");

        String gettersstring="";
        String comment="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            if(i==0)comment="";//else comment="//";
            gettersstring+=
                "\n"+comment+"  class func getBy"+toCamelCase(field)+"("+field+": "+datatypeFor(fieldtypes.get(i))+")->[tableCaps]?"
                +"\n"+comment+"  {"
                +"\n"+comment+"    let result=[tableCapsPlural].select(\"where "+field+" = '\\("+field+")'\")"
                +"\n"+comment+"    if(result.count>0){return result[0]}"
                +"\n"+comment+"    else {return nil}"
                +"\n"+comment+"  }";
        }
                
        String lastinsertid="";
        if(fieldtypes.get(0).contains("int"))lastinsertid
+="\n  class func getLastInsertId()->Int"
+"\n  {"
+"\n        let db=DatabaseHelper.getInstance()"
+"\n        if db == nil{return -1}"
+"\n"
+"\n        let mainQuery = \"SELECT last_insert_rowid() from \"+Static.tablename"
+"\n        let rsMain: FMResultSet? = db!.executeQuery(mainQuery, withArgumentsInArray: [])"
+"\n "
+"\n        if (rsMain?.next() == true) {"
+"\n            var temp=rsMain?.intForColumn(\"last_insert_rowid()\")"
+"\n            return String(temp!).toInt()!"
+"\n        }"
+"\n        else {return -1}"
+"\n  }";
        
        String syncinstring="";
        for(int i=1;i<fields.size();i++)
        {
            field=fields.get(i);
            if(field.contentEquals("posted"))
            {
                syncinstring+="\n				item.posted=1";
            }
            else if(fieldtypes.get(i).contains("varchar") || (fieldtypes.get(i).contains("text") && !fieldtypes.get(i).contentEquals("text")) )
            {
//syncinstring+="\n				if(attribs[\""+field+"\"] != nil){item.name=attribs[\""+field+"\"].replace(\"'\", \"''\")}";
syncinstring+="\n				if let temp = (attribs[\""+field+"\"] as? "+datatypeFor(fieldtypes.get(i))+"){item."+field+"=temp.stringByReplacingOccurrencesOfString(\"'\", withString: \"''\")}";
				

            }
            else
            {
//syncinstring+="\n				if(attribs[\""+field+"\"] != nil){item.name=attribs[\""+field+"\"]}";
syncinstring+="\n				if let temp = (attribs[\""+field+"\"] as? "+datatypeFor(fieldtypes.get(i))+"){item."+field+"=temp}";
            }
        }
        
        //function syncIn is only present if id is string
        String syncinfuncstring="";
if(fieldtypes.get(0).contains("varchar"))//no such thing as a text or longtext id
{syncinfuncstring=
"\n  class func syncIn(update: NSDictionary, errbank: ErrorBank)"
+"\n  {"
+"\n    switch(update[\"action\"] as String)"
+"\n    {"
+"\n      case \"create\":"
+"\n        var attribs=update[\"new_or_updated_attributes\"] as NSDictionary"
+"\n				var item=[tableCaps]()"
+"\n				item."+fields.get(0)+"=update[\"entity_uuid\"] as String"
+syncinstring
+"\n				item.save()"
+"\n        break;"
+"\n      case \"update\":"
+"\n        var attribs=update[\"new_or_updated_attributes\"] as NSDictionary"
+"\n				if var item=[tableCapsPlural].getById(update[\"entity_uuid\"] as String)"
+"\n				{"
+syncinstring
+"\n				item.save()"
+"\n				}"
+"\n        else"
+"\n        {"
+"\n          let log_num=update[\"log_num\"] as Int"
+"\n          errbank.addError(\"Error processing update: Log Id \"+String(log_num)+\": record not found\")"
+"\n        }"
+"\n      case \"delete\":if var item=[tableCapsPlural].getById(update[\"entity_uuid\"] as "+datatypeFor(fieldtypes.get(0))+"){item.delete()};break;"
+"\n      default: break;"
+"\n    }"
+"\n  }";
}
                
        
        String output="import Foundation"
+"\n"
+"\nclass [tableCapsPlural] {"
+"\n  struct Static"
+"\n  {"
+"\n    static let tablename=\"[table]\""
+"\n    static let fields=["+fieldsstring+"]"
+"\n    static let fieldstypes=["+fieldtypesstring+"]"
+"\n  }"
+"\n    //---getters----"
+gettersstring
//+"\n  class func getById(id: "+datatypeFor(fieldtypes.get(0))+")->[tableCaps]"
//+"\n  {"
//+"\n    return [tableCapsPlural].select(\"where id = '\\(id)'\")[0]"
//+"\n  }"
+"\n    //---standard db functions----"
+"\n  class func delete(item: [tableCaps])->Bool"
+"\n  {"
+"\n        let db=DatabaseHelper.getInstance()"
+"\n        if db == nil{println(\"db nil\");return false}"
+"\n"
+"\n        // delete data"
+"\n        let delQuery = \"DELETE FROM \\(Static.tablename) WHERE id = '\\(item.id)' \""
+"\n        let deleteSuccessful = db!.executeUpdate(delQuery, withArgumentsInArray: nil)"
+"\n        if !deleteSuccessful {"
+"\n            println(\"delete failed: \\(db!.lastErrorMessage())\")"
+"\n        }"
+"\n       return deleteSuccessful"
+"\n  }"
+lastinsertid                
+"\n  class func select(criteria: String)->[[tableCaps]]"
+"\n  {"
+"\n        let db=DatabaseHelper.getInstance()"
+"\n        if db == nil{return []}"
+"\n"
+"\n        let mainQuery = \"SELECT * from \"+Static.tablename+\" \"+criteria"
+"\n        let rsMain: FMResultSet? = db!.executeQuery(mainQuery, withArgumentsInArray: [])"
+"\n "
+"\n        var list: [[tableCaps]]=[]"
+"\n    "
+"\n        while (rsMain?.next() == true) {"
+"\n            list.append([tableCaps](rsMain: rsMain!))"
+"\n        }"
+"\n        return list"
+"\n  }"
+"\n  class func selectOne(criteria: String)->[tableCaps]"
+"\n  {"
+"\n    return [tableCapsPlural].select(criteria+\" limit 1\")[0]"
+"\n  }"
+"\n  class func count(criteria: String)->Int"
+"\n  {"
+"\n        let db=DatabaseHelper.getInstance()"
+"\n        if db == nil{return 0}"
+"\n"
+"\n        // example: get num rows"
+"\n        let rsTemp: FMResultSet? = db!.executeQuery(\"SELECT count(*) AS numrows from \"+Static.tablename+\" \"+criteria, withArgumentsInArray: [])"
+"\n        rsTemp!.next()"
+"\n        return String(rsTemp!.intForColumn(\"numrows\")).toInt()!"
+"\n  }"
+"\n  class func insert(item: [tableCaps])->Bool"
+"\n  {"
+"\n        let db=DatabaseHelper.getInstance()"
+"\n        if db == nil{return false}"
+"\n"
+"\n        // insert data"
+"\n        let addQuery = \"INSERT INTO \\(Static.tablename) (\\(commaSeparatedFields(includeId: "+hasstringid+"))) VALUES (\\(item.commaSeparatedValues(includeId: "+hasstringid+")))\""
+"\n        let addSuccessful = db!.executeUpdate(addQuery, withArgumentsInArray: nil)"
+"\n        if !addSuccessful {"
+"\n            println(\"insert failed: \\(db!.lastErrorMessage())\")"
+"\n        }"
+"\n        return addSuccessful"
+"\n  }"
+"\n  class func update(item: [tableCaps])->Bool"
+"\n  {"
+"\n        let db=DatabaseHelper.getInstance()"
+"\n        if db == nil{return false}"
+"\n"
+"\n        // update data"
+"\n        let updateQuery = \"UPDATE \\(Static.tablename) SET \\(item.commaSeparatedFieldsAndValues(includeId: false)) WHERE id = '\\(item.id)' \""
+"\n        let updateSuccessful = db!.executeUpdate(updateQuery, withArgumentsInArray: nil)"
+"\n        if !updateSuccessful {"
+"\n            println(\"update failed: \\(db!.lastErrorMessage())\")"
+"\n        }"
+"\n        return updateSuccessful"
+"\n  }"
+"\n  "
+"\n    //---helpers----"
+"\n  class func commaSeparatedFields(includeId: Bool = true)->String"
+"\n  {"
+"\n    var index: Int=0"
+"\n    if includeId==false{index=1}"
+"\n    var string=Static.fields[index]"
+"\n    index++"
+"\n    for ; index < Static.fields.count; ++index {"
+"\n      string+=\",\"+Static.fields[index]"
+"\n    }"
+"\n    return string"
+"\n  }"
+"\n  class func test()"
+"\n  {"
+"\n        println(\"[tableCapsPlural] test\")"
+"\n"
+"\n        //count"
+"\n        println(String([tableCapsPlural].count(\"\"))+\" records in table\")"
+"\n"
+"\n        //create"
+"\n        println(\"create\")"
+"\n        var item=[tableCaps]()"
+testdata1
+"\n        if(item.save()){println(\"insert success\")}"
+"\n        println(item."+fields.get(0)+")"
+"\n"
+"\n        //count again"
+"\n        println(String([tableCapsPlural].count(\"\"))+\" records in table\")"
+"\n"
+"\n        //update"
+"\n        println(\"update\")"
+testdata2
+"\n        if(item.save()){println(\"update success\")}"
+"\n"
+"\n        //delete"
+"\n        println(\"delete\")"
+"\n        if(item.delete()){println(\"delete success\")}"
+"\n        println(String([tableCapsPlural].count(\"\"))+\" records in table\")"
+"\n  }"
+syncinfuncstring
+"\n}"
;
                
                
//        String output="package models;"
//+"\n"
//+"\nimport java.io.IOException;"
//+"\nimport java.math.BigDecimal;"
//+"\nimport java.sql.Connection;"
//+"\nimport java.sql.ResultSet;"
//+"\nimport java.sql.SQLException;"
//+"\nimport java.sql.Statement;"
//+"\nimport java.sql.Date;"
//+"\nimport java.sql.Timestamp;"
//+"\nimport java.util.ArrayList;"
//+"\nimport java.util.logging.Level;"
//+"\nimport java.util.logging.Logger;"
//+"\nimport utils.MySqlDBHelper;"
//+"\nimport utils.JsonHelper;"
//+"\n"
//+"\npublic class [tableCapsPlural] {"
//+"\n    //------------FIELDS-----------"
//+"\n    public static final String tablename=[tableCaps].tablename;"
//+"\n    public static String[] fields=[tableCaps].fields;"
//+"\n    public static String[] fieldtypes=[tableCaps].fieldtypes;"
//+"\n    //-----------------------"
//+"\n    //-------------------------TABLE FUNCTIONS---------------------"
//+"\n"
//+"\n    //-----------getter functions----------"
//+"\n    /*"
//+"\n    public static [tableCapsPlural] getByName(String name)"
//+"\n    {"
//+"\n            HashMap<"+iddatatype+",[tableCapsPlural]> map=select(\" name = '\"+name+\"'\");"
//+"\n            for([tableCapsPlural] item:map)return item;"
//+"\n            return null;"
//+"\n    }	"
//+"\n    */"
//+"\n    public static [tableCaps] getBy"+toCamelCase(idfield)+"("+iddatatype+" "+idfield+") {"
//+"\n            RecordList map=select(\" "+idfield+" = '\"+"+idfield+idfieldtypestringifier+"+\"'\");"
//+"\n            for([tableCaps] item:map)return item;"
//+"\n            return null;"
//+"\n    }"
//+"\n    //-----------database functions--------------"
//+"\n"
//+"\n    public static void delete("+iddatatype+" "+idfield+")"
//+"\n    {"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();            "
//+"\n        Statement st = null;"
//+"\n        try { "
//+"\n            st = conn.createStatement();"
//+"\n            st.executeUpdate(\"delete from \"+tablename+\" where "+idfield+" = '\"+"+idfield+idfieldtypestringifier+"+\"';\");"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n        }"
//+"\n    }"
//+"\n    public static void delete([tableCaps] item)"
//+"\n    {"
//+"\n        delete(item.get"+toCamelCase(idfield)+"());"
//+"\n    }"
//+"\n    public static void insert([tableCaps] item)"
//+"\n    {"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();            "
//+"\n        Statement st = null;"
//+"\n        boolean withid=false;"
//+"\n        try { "
//+"\n            st = conn.createStatement();"
//+"\n            //for tables with integer primary key"
//+"\n            if(fieldtypes[0].contentEquals(\"integer\"))withid=false;                "
//+"\n            //for tables with varchar primary key"
//+"\n            else if(fieldtypes[0].contains(\"varchar\"))withid=true;                "
//+"\n            st.executeUpdate(\"INSERT INTO \"+tablename+\" (\"+implodeFields(withid)+\")VALUES (\"+implodeValues(item, withid)+\");\");"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n        }"
//+"\n    }"
//+"\n    public static void update([tableCaps] item)"
//+"\n    {"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();            "
//+"\n        Statement st = null;"
//+"\n        boolean withid=false;"
//+"\n        try { "
//+"\n            st = conn.createStatement();"
//+"\n            st.executeUpdate(\"update \"+tablename+\" set \"+implodeFieldsWithValues(item,false)+\" where "+idfield+" = '\"+item.get"+toCamelCase(idfield)+"()"+idfieldtypestringifier+"+\"';\");"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n        }"
//+"\n    }"
//+"\n    public static Integer count(String conditions)"
//+"\n    {"
//+"\n        if(conditions.isEmpty())conditions = \"1\";"
//+"\n"
//+"\n        //if conditions contains a limit clause, remove it. "
//+"\n        //It is not applicable to a count query"
//+"\n        else if(conditions.contains(\"limit\"))"
//+"\n        {"
//+"\n            String[] segments=conditions.split(\"limit\");"
//+"\n            conditions=segments[0];"
//+"\n        }"
//+"\n"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();"
//+"\n        Statement st = null;"
//+"\n        ResultSet rs = null;"
//+"\n        try { "
//+"\n        st = conn.createStatement();"
//+"\n        rs = st.executeQuery(\"SELECT count(*) from \"+tablename+\" where \"+conditions);"
//+"\n            while (rs.next()) {"
//+"\n                return rs.getInt(1);"
//+"\n            }"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n        }"
//+"\n        return null;"
//+"\n    }"
//+"\n    public static RecordList select(String conditions)"
//+"\n    {"
//+"\n        if(conditions.isEmpty())conditions = \"1\";"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();"
//+"\n        Statement st = null;"
//+"\n        ResultSet rs = null;"
//+"\n        try { "
//+"\n            st = conn.createStatement();"
//+"\n                rs = st.executeQuery(\"SELECT * from \"+tablename+\" where \"+conditions);"
//+"\n"
//+"\n            RecordList items=new RecordList();"
//+"\n            while (rs.next()) {"
//+"\n                items.add(new [tableCaps](rs));"
//+"\n                    //items.put(rs."+rsGetterFor(idfieldtype)+"(\""+idfield+"\"), new [tableCapsPlural](rs));"
//+"\n            }"
//+"\n            return items;"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n            return null;"
//+"\n        }"
//+"\n    }"
//+"\n"
//+"\n    //-----------database helper functions--------------"
//+"\n    public static String implodeValues([tableCaps] item,boolean withId)"
//+"\n    {"
//+"\n            ArrayList<String> values=item.implodeFieldValuesHelper(withId);"
//+"\n            String output=\"\";"
//+"\n            for(String value:values)"
//+"\n            {"
//+"\n                    if(!output.isEmpty())"
//+"\n                            output+=\",\";"
//+"\n                    output+=(value!=null?\"'\"+value+\"'\":\"null\");"
//+"\n            }"
//+"\n            return output;"
//+"\n    }"
//+"\n    public static String implodeFields(boolean withId)"
//+"\n    {"
//+"\n            String output=\"\";"
//+"\n            for(String field:fields)"
//+"\n            {"
//+"\n                    if(!withId && field.contentEquals(\""+idfield+"\"))continue;"
//+"\n                    if(!output.isEmpty())"
//+"\n                            output+=\",\";"
//+"\n                    output+=field;"
//+"\n            }"
//+"\n            return output;"
//+"\n    }"
//+"\n    public static String implodeFieldsWithValues([tableCaps] item,boolean withId)"
//+"\n    {"
//+"\n            ArrayList<String> values=item.implodeFieldValuesHelper(true);//get entire list of values; whether the id is included will be dealt with later."
//+"\n"
//+"\n            if(values.size()!=fields.length)"
//+"\n            {"
//+"\n                    System.err.println(\"[tableCapsPlural]:implodeFieldsWithValues(): ERROR: values length does not match fields length\");"
//+"\n            }"
//+"\n"
//+"\n            String output=\"\";"
//+"\n            for(int i=0;i<fields.length;i++)"
//+"\n            {"
//+"\n                    if(!withId && fields[i].contentEquals(\""+idfield+"\"))continue;"
//+"\n                    if(!output.isEmpty())"
//+"\n                            output+=\",\";"
//+"\n                    output+=fields[i]+\"=\"+(values.get(i)!=null?\"'\"+values.get(i)+\"'\":\"null\");"
//+"\n            }"
//+"\n            return output;"
//+"\n    }	"
//+"\n    public static String implodeFieldsWithTypes()"
//+"\n    {"
//+"\n            String output=\"\";"
//+"\n            for(int i=0;i<fields.length;i++)"
//+"\n            {"
//+"\n                    if(fields[i].contentEquals(fields[0]))//fields[0] being the primary key"
//+"\n                            output+=fields[i]+\" \"+fieldtypes[i]+\" PRIMARY KEY\";"
//+"\n                    else"
//+"\n                            output+=\",\"+fields[i]+\" \"+fieldtypes[i];"
//+"\n            }"
//+"\n            return output;"
//+"\n    }	"
//+"\n    public static String createTable()"
//+"\n    {"
//+"\n            return \"CREATE TABLE IF NOT EXISTS \"+tablename+\" (\"+implodeFieldsWithTypes()+\" );\";"
//+"\n    }"
//+"\n    public static String deleteTable()"
//+"\n    {"
//+"\n            return \"DROP TABLE IF EXISTS \"+tablename;"
//+"\n    }"
//+"\n    public static class RecordList extends ArrayList<[tableCaps]>{"
//+"\n        public static RecordList fromJsonString(String resultstring) throws IOException"
//+"\n        {"
//+"\n            return JsonHelper.mapper.readValue(resultstring, RecordList.class);"
//+"\n        }"
//+"\n        public String toEscapedJsonString() throws IOException"
//+"\n        {"
//+"\n            return \"\\\"\"+JsonHelper.mapper.writeValueAsString(this).replace(\"\\\"\", \"\\\\\\\"\") +\"\\\"\";"
//+"\n        }"
//+"\n    }"
//+"\n    public static void main(String args[])"
//+"\n    {"
//+"\n        String database=\""+database+"\";"
//+"\n        String url = \"jdbc:mysql://"+hostname+":3306/\"+database+\"?zeroDateTimeBehavior=convertToNull\";"
//+"\n        String username=\""+username+"\";"
//+"\n        String password = \""+password+"\";"
//+"\n"
//+"\n        boolean result=MySqlDBHelper.init(url, username, password);            "
//+"\n"
//+"\n        RecordList items=[tableCapsPlural].select(\"\");"
//+"\n        for([tableCaps] item:items)"
//+"\n        {"
//+"\n            System.out.println(item);"
//+"\n        }"
//+"\n        System.out.println([tableCapsPlural].count(\"\"));"
//+"\n    } "
//+"\n}"
//+"\n";
        String tableplural=table;
        String tablesingular=singularize(table);
        String tablecapssingular=toCamelCase(tablesingular);
        String tablecapsplural=toCamelCase(tableplural);
        output=output.replace("[tableCaps]", tablecapssingular);
        output=output.replace("[table]", table);
        output=output.replace("[tableCapsPlural]", tablecapsplural);
        output=output.replace("[tablePlural]", tableplural);

        return output;
    }
    public static void createModelFile(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        //write file to current directory
        File outputdir=new File("./src/models");
        outputdir.mkdir();
        FileWriter.write(outputdir.getPath()+"/"+toCamelCase(singularize(table))+".swift", createModelFileContents(table,fields,fieldtypes));
    }
    public static String createModelFileContents(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        int counter=0;
        
//        String fieldsstring="";
//        for(int i=0;i<fields.size();i++)
//        {
//            fieldsstring+="\n            "+(i==0?"":",")+"\""+fields.get(i) +"\"";
//        }
//        
//        String fieldtypesstring="";
//        for(int i=0;i<fieldtypes.size();i++)
//        {
//            fieldtypesstring+="\n            "+(i==0?"":",")+"\""+fieldtypes.get(i) +"\"";
//        }
//        
        String idfield=fields.get(0);
        String idfieldtype=fieldtypes.get(0);
        String idfieldtypestringifier=stringifier(fieldtypes.get(0));
        String iddatatype=datatypeFor(fieldtypes.get(0));
        
        String gettersandsetters="";
        String datatype="";
        String field="";
        String fieldtype="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            datatype=datatypeFor(fieldtypes.get(i));
//            gettersandsetters+=
//"\n    public "+datatype+" get"+toCamelCase(field)+"() {"
//+"\n            return "+field+";"
//+"\n    }"
//+"\n"
//+"\n    public void set"+toCamelCase(field)+"("+datatype+" "+field+") {"
//+"\n            this."+field+" = "+field+";"
//+"\n    }"
//+"\n";
//        }
//        
/*
    var id: Int = 0
    var studentRollNo: String = String()
    var studentName: String = String()  
*/
        String vardefinitions="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            datatype=datatypeFor(fieldtypes.get(i));
            vardefinitions+=
"\n    var "+field+": "+datatype+" = "+datatypeInitialValueFor(fieldtypes.get(i));
        }
                        
/*
    self.id=rsMain.intForColumn("id")
    if let temp = rsMain.stringForColumn("student_rollno")
    {
      self.studentRollNo=temp
    }
    if let temp = rsMain.stringForColumn("student_name")
    {
      self.studentName=temp
    }
 */
        String constructorfields="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            if(fieldtypes.get(i).contains("varchar"))
            {
                constructorfields+="\n    if let temp = rsMain."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\"){self."+field+"=temp}";
            }
            else if(fieldtypes.get(i).contentEquals("date"))
            {
                constructorfields+="\n    if let temp = rsMain.stringForColumn(\""+field+"\"){self."+field+"=DateHelper.toDate(temp)}";
            }
            else if(fieldtypes.get(i).contentEquals("text"))
            {
                constructorfields+="\n    if let temp = rsMain."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\"){"+field+"=DatabaseHelper.parseCommaSeparatedString(temp)}";
            }
            else if(fieldtypes.get(i).contains("int")&&!fieldtypes.get(i).contains("tiny")&&!fieldtypes.get(i).contains("small"))
            {
                constructorfields+="\n    self."+field+" = String(rsMain."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\")).toInt()!";
            }
            else
            {
                constructorfields+="\n    self."+field+" = rsMain."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\")";
            }
        }
 
/*
    if includeId {string+="'"+String(id)+"',"}
    string+="'"+studentRollNo+"'"
    string+=","+"'"+studentName+"'"
  */
        String implodevaluesstring="";
        String comma="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            fieldtype=fieldtypes.get(i);
            if(i==0)
            {
                implodevaluesstring+=
"\n    if includeId {"
+"\n        string+=\"'\"+"+stringifiedWithNull(field,fieldtype)+"+\"',\""
+"\n    }";
//"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
            }
            else
            {
                if(i==1)comma="";else comma=",";
                implodevaluesstring+=
"\n    string+=\""+comma+"'\"+"+stringifiedWithNull(field,fieldtype)+"+\"'\"";
//"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
            }
        }

        String implodefieldsandvaluesstring="";
        for(int i=0;i<fields.size();i++)
        {
            field=fields.get(i);
            fieldtype=fieldtypes.get(i);
            if(i==0)
            {
                implodefieldsandvaluesstring+=
"\n    if includeId {"
+"\n        string+=\""+field+"='\"+"+stringifiedWithNull(field,fieldtype)+"+\"',\""
+"\n    }";
//"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
            }
            else
            {
                if(i==1)comma="";else comma=",";
                implodefieldsandvaluesstring+=
"\n    string+=\""+comma+field+"='\"+"+stringifiedWithNull(field,fieldtype)+"+\"'\"";
//"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
            }
        }
        
        String savestring="";
        if(iddatatype.contentEquals("String"))
            savestring=
            "\n    if("+idfield+"==\"\"){id=UuidGenerator.generate()}"
            +"\n    if(!_saved)"
            +"\n    {"
            +"\n      _saved=true"
            +"\n      return [tableCapsPlural].insert(self)"
            +"\n    }";
        else
            savestring=
            "\n    if(!_saved)"
            +"\n    {"
            +"\n      _saved=true"
            +"\n      let result=[tableCapsPlural].insert(self)"
            +"\n      if(result){id=[tableCapsPlural].getLastInsertId()}"
            +"\n      return result"
            +"\n    }"
                    ;
//        if(iddatatype.contentEquals("String"))
//            savestring="\n            if("+idfield+"==null || "+idfield+".isEmpty() )";
//        else
//            savestring="\n            if("+idfield+"==null || "+idfield+"==0)";
        
        String output="import UIKit"
+"\n"
+"\nclass [tableCaps] {"
+vardefinitions
+"\n    var _saved: Bool = false  //saved to database or not"
+"\n  "
+"\n  //constructors"
+"\n  init(){}"
+"\n  init(rsMain: FMResultSet)"
+"\n  {"
+"\n    //obtained from database: saved is true"
+"\n    _saved=true"
+constructorfields
+"\n  }"
+"\n"
+"\n  //helpers"
+"\n  func commaSeparatedValues(includeId: Bool = true)->String"
+"\n  {"
+"\n    var string=\"\""
+"\n"
+implodevaluesstring
+"\n    "
+"\n    return string"
+"\n  }"
+"\n  func commaSeparatedFieldsAndValues(includeId: Bool = true)->String"
+"\n  {"
+"\n    var string=\"\""
+"\n"
+implodefieldsandvaluesstring
+"\n    "
+"\n    return string"
+"\n  }"
+"\n"
+"\n"
+"\n  func delete()->Bool"
+"\n  {"
+"\n    return [tableCapsPlural].delete(self)"
+"\n  }"
+"\n  func save()->Bool"
+"\n  {"
+savestring
+"\n    else"
+"\n    {"
+"\n      return [tableCapsPlural].update(self)"
+"\n    }"
+"\n  }"
+"\n}";

        String tableplural=table;
        String tablesingular=singularize(table);
        String tablecapssingular=toCamelCase(tablesingular);
        String tablecapsplural=toCamelCase(tableplural);
        output=output.replace("[tableCaps]", tablecapssingular);
        output=output.replace("[table]", table);
        output=output.replace("[tableCapsPlural]", tablecapsplural);
        output=output.replace("[tablePlural]", tableplural);
        
        return output;
    }
    private static String singularize(String table)
    {
        String singular=table;
        if(table.substring(table.length()-3, table.length()).contentEquals("ies"))
        {
            singular=table.substring(0,table.length()-3)+"y";
        }
        else singular=table.substring(0,table.length()-1);
        return singular;
    }
    
    //-----utils-----
    public static String toCamelCase(String string)
    {
        String[] segments=string.split("_");
        String output="";
        for(String s:segments)
            output+=capitalize(s);
        return output;
    }
    public static String capitalize(String s)
    {
        return s.substring(0, 1).toUpperCase()+s.substring(1);
    }
    public static String datatypeFor(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        //System.out.println(type);
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "Int";
        else if(type.contentEquals("varchar()"))
            return "String";
        else if(type.contentEquals("char()"))
            return "String";
        else if(type.contentEquals("text"))
            return "[String]";
        else if(type.contentEquals("tinytext"))
            return "String";
        else if(type.contentEquals("mediumtext"))
            return "String";
        else if(type.contentEquals("longtext"))
            return "String";
        else if(type.contentEquals("date"))
            return "NSDate";
        else if(type.contains("bigint"))
            return "Long";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "Bool";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "BigDecimal";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "Float";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "Double";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "Boolean";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "Timestamp";
        else if(type.contains("enum"))
            return "String";
        else 
            return "";
/*
<option value="INT" selected="selected">INT</option>
<option value="VARCHAR">VARCHAR</option>
<option value="TEXT">TEXT</option>
<option value="DATE">DATE</option>
<optgroup label="NUMERIC"><option value="TINYINT">TINYINT</option>
<option value="SMALLINT">SMALLINT</option>
<option value="MEDIUMINT">MEDIUMINT</option>
<option value="INT" selected="selected">INT</option>
<option value="BIGINT">BIGINT</option>
<option value="-">-</option>
<option value="DECIMAL">DECIMAL</option>
<option value="FLOAT">FLOAT</option>
<option value="DOUBLE">DOUBLE</option>
<option value="REAL">REAL</option>
<option value="-">-</option>
<option value="BIT">BIT</option>
<option value="BOOLEAN">BOOLEAN</option>
<option value="SERIAL">SERIAL</option>
</optgroup><optgroup label="DATE and TIME"><option value="DATE">DATE</option>
<option value="DATETIME">DATETIME</option>
<option value="TIMESTAMP">TIMESTAMP</option>
<option value="TIME">TIME</option>
<option value="YEAR">YEAR</option>
</optgroup><optgroup label="STRING"><option value="CHAR">CHAR</option>
<option value="VARCHAR">VARCHAR</option>
<option value="-">-</option>
<option value="TINYTEXT">TINYTEXT</option>
<option value="TEXT">TEXT</option>
<option value="MEDIUMTEXT">MEDIUMTEXT</option>
<option value="LONGTEXT">LONGTEXT</option>
<option value="-">-</option>
<option value="BINARY">BINARY</option>
<option value="VARBINARY">VARBINARY</option>
<option value="-">-</option>
<option value="TINYBLOB">TINYBLOB</option>
<option value="MEDIUMBLOB">MEDIUMBLOB</option>
<option value="BLOB">BLOB</option>
<option value="LONGBLOB">LONGBLOB</option>
<option value="-">-</option>
<option value="ENUM">ENUM</option>
<option value="SET">SET</option>
</optgroup><optgroup label="SPATIAL"><option value="GEOMETRY">GEOMETRY</option>
<option value="POINT">POINT</option>
<option value="LINESTRING">LINESTRING</option>
<option value="POLYGON">POLYGON</option>
<option value="MULTIPOINT">MULTIPOINT</option>
<option value="MULTILINESTRING">MULTILINESTRING</option>
<option value="MULTIPOLYGON">MULTIPOLYGON</option>
<option value="GEOMETRYCOLLECTION">GEOMETRYCOLLECTION</option>
</optgroup>    
 */        
    }
    public static String datatypeInitialValueFor(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        //System.out.println(type);
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "0";
        else if(type.contentEquals("varchar()"))
            return "\"\"";
        else if(type.contentEquals("char()"))
            return "\"\"";
        else if(type.contentEquals("text"))
            return "[]";
        else if(type.contentEquals("tinytext"))
            return "\"\"";
        else if(type.contentEquals("mediumtext"))
            return "\"\"";
        else if(type.contentEquals("longtext"))
            return "\"\"";
        else if(type.contentEquals("date"))
            return "DateHelper.getEmptyDate()";
        else if(type.contains("bigint"))
            return "0";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "false";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "0";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "0";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "0";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "false";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "Timestamp";
        else if(type.contains("enum"))
            return "String";
        else 
            return "";
    }
    public static String rsGetterFor(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "intForColumn";
        else if(type.contains("varchar()"))
            return "stringForColumn";
        else if(type.contentEquals("char()"))
            return "stringForColumn";
        else if(type.contentEquals("text"))
            return "stringForColumn";
        else if(type.contentEquals("tinytext"))
            return "stringForColumn";
        else if(type.contentEquals("mediumtext"))
            return "stringForColumn";
        else if(type.contentEquals("longtext"))
            return "stringForColumn";
        else if(type.contentEquals("date"))
            return "getDate";
        else if(type.contains("bigint"))
            return "getLong";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "boolForColumn";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "getBigDecimal";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "getFloat";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "getDouble";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "getBoolean";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "getTimestamp";
        else if(type.contains("enum"))
            return "getString";
        else 
            return "";  
    }    
    public static String stringifier(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return ".toString()";
        else if(type.contains("varchar()"))
            return "";
        else if(type.contentEquals("char()"))
            return "";
        else if(type.contentEquals("text"))
            return "";
        else if(type.contentEquals("tinytext"))
            return "";
        else if(type.contentEquals("mediumtext"))
            return "";
        else if(type.contentEquals("longtext"))
            return "";
        else if(type.contentEquals("date"))
            return ".toString()";
        else if(type.contains("bigint"))
            return ".toString()";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return ".toString()";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return ".toString()";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return ".toString()";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return ".toString()";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return ".toString()";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return ".toString()";
        else if(type.contains("enum"))
            return "";
        else 
            return "";  
    }       
    public static String stringifiedWithNull(String field,String type)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "String("+field+")";
        else if(type.contains("varchar()"))
            return field;
        else if(type.contentEquals("char()"))
            return field;
        else if(type.contentEquals("text"))
            return "DatabaseHelper.mergeCommaSeparatedString("+field+")";
        else if(type.contentEquals("tinytext"))
            return field;
        else if(type.contentEquals("mediumtext"))
            return field;
        else if(type.contentEquals("longtext"))
            return field;
        else if(type.contentEquals("date"))
            return "DateHelper.toString(date)";
        else if(type.contains("bigint"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "("+field+" ? \"1\" : \"0\")";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contains("enum"))
            return field;
        else 
            return field;  
    }    

}
