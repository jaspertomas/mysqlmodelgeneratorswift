/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mysqlmodelgenerator;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import utils.MySqlDBHelper;
import utils.fileaccess.FileWriter;

/**
 *
 * @author jaspertomas
 */
public class MysqlOutgoingSyncHelperGenerator {
    
    static String database="database";
    static String hostname="localhost";
    static String username="root";
    static String password="password";

    public static void main(String args[])
    {
        String url = "jdbc:mysql://"+hostname+":3306/"+database;

        MySqlDBHelper.init(url, username, password);            
        Connection conn=MySqlDBHelper.getInstance().getConnection();
        
        Statement st = null;
        ResultSet rs = null;
        ArrayList<String> tables=new ArrayList<String>();
        ArrayList<String> fields=new ArrayList<String>();
        ArrayList<String> fieldtypes=new ArrayList<String>();
        try { 
            st = conn.createStatement();
            rs = st.executeQuery("Show Tables");

            while (rs.next()) {
                tables.add(rs.getString(1));
            }
            for(String table:tables) {
                rs = st.executeQuery("SHOW COLUMNS FROM "+table);
                fields.clear();
                fieldtypes.clear();
                while (rs.next()) {
                    fields.add(rs.getString(1));
                    fieldtypes.add(rs.getString(2));
                }

                //only if field named posted exists
                if(fields.contains("posted"))
                createModelsFile(table,fields,fieldtypes);
                
                //create code for DatabaseHelper.dump()
//                System.out.println("for item in "+toCamelCase(table)+".select(\"\"){println(\""+toCamelCase(table)+" \"+item.commaSeparatedFieldsAndValues(includeId: true));println();}");
  
            //create SyncInHelper's processUpdate() switch statement
            //includes only tables with string id's, as only those are syncable
            //sample output:
            //      case "Apps": Apps.syncIn(update,errbank: errbank);break;
//                if(!fieldtypes.get(0).contains("int"))
//                    System.out.println("      case \""+toCamelCase(singularize(table))+"\": "+toCamelCase(table)+".syncIn(update,errbank: errbank);break;");
            
            }
        } catch (SQLException ex) {
            //Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public static void createModelsFile(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
        //write file to current directory
        File outputdir=new File("./src/models");
        outputdir.mkdir();
        FileWriter.write(outputdir.getPath()+"/"+toCamelCase(table)+"OutSyncHelper.swift", createModelsFileContents(table,fields,fieldtypes));
    }
    public static String createModelsFileContents(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
    {
//        int counter=0;
//        
//        String fieldsstring="";
//        for(int i=0;i<fields.size();i++)
//        {
//            fieldsstring+="\n            "+(i==0?"":",")+"\""+fields.get(i) +"\"";
//        }
//        
//        String fieldtypesstring="";
//        for(int i=0;i<fieldtypes.size();i++)
//        {
//            fieldtypesstring+="\n            "+(i==0?"":",")+"\""+datatypeFor(fieldtypes.get(i))+"\"";
//        }
//        
//        String idfield=fields.get(0);
//        String idfieldtype=fieldtypes.get(0);
//        String idfieldtypestringifier=stringifier(fieldtypes.get(0));
//        String iddatatype=datatypeFor(fieldtypes.get(0));
//        
//        String gettersandsetters="";
//        String datatype="";
//        String field="";
//        String fieldtype="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            datatype=datatypeFor(fieldtypes.get(i));
//            gettersandsetters+=
//"\n    public "+datatype+" get"+toCamelCase(field)+"() {"
//+"\n            return "+field+";"
//+"\n    }"
//+"\n"
//+"\n    public void set"+toCamelCase(field)+"("+datatype+" "+field+") {"
//+"\n            this."+field+" = "+field+";"
//+"\n    }"
//+"\n";
//        }
//        
//        String vardefinitions="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            datatype=datatypeFor(fieldtypes.get(i));
//            vardefinitions+=
//"\n    public "+datatype+" "+field+";";
//        }
//                        
//        String constructorfields="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            constructorfields+=
//"\n            "+field+"=rs."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\");";
//        }
// 
//        String implodevaluesstring="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            fieldtype=fieldtypes.get(i);
//            implodevaluesstring+=
//"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
//        }
        
//        String savestring="";
//        if(iddatatype.contentEquals("String"))
//            savestring="\n            if("+idfield+"==null || "+idfield+".isEmpty() )";
//        else
//            savestring="\n            if("+idfield+"==null || "+idfield+"==0)";
        
//        String testdata1="";
//        if(fieldtypes.get(1).contains("varchar"))testdata1="\n        item."+fields.get(1)+"=\"ab\"";
//        else if(fieldtypes.get(1).contains("int"))testdata1="\n        item."+fields.get(1)+"=1";
//        String testdata2="";
//        if(fieldtypes.get(1).contains("varchar"))testdata2="\n        item."+fields.get(1)+"=\"cd\"";
//        else if(fieldtypes.get(1).contains("int"))testdata2="\n        item."+fields.get(1)+"=2";
//        
//        String hasstringid=(fieldtypes.get(0).contains("varchar")?"true":"false");
//
//        String gettersstring="";
//        String comment="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            if(i==0)comment="";//else comment="//";
//            gettersstring+=
//                "\n"+comment+"  class func getBy"+toCamelCase(field)+"("+field+": "+datatypeFor(fieldtypes.get(i))+")->[tableCaps]?"
//                +"\n"+comment+"  {"
//                +"\n"+comment+"    let result=[tableCapsPlural].select(\"where "+field+" = '\\("+field+")'\")"
//                +"\n"+comment+"    if(result.count>0){return result[0]}"
//                +"\n"+comment+"    else {return nil}"
//                +"\n"+comment+"  }";
//        }
//                
//        String lastinsertid="";
//        if(fieldtypes.get(0).contains("int"))lastinsertid
//+="\n  class func getLastInsertId()->Int"
//+"\n  {"
//+"\n        let db=DatabaseHelper.getInstance()"
//+"\n        if db == nil{return -1}"
//+"\n"
//+"\n        let mainQuery = \"SELECT last_insert_rowid() from \"+Static.tablename"
//+"\n        let rsMain: FMResultSet? = db!.executeQuery(mainQuery, withArgumentsInArray: [])"
//+"\n "
//+"\n        if (rsMain?.next() == true) {"
//+"\n            var temp=rsMain?.intForColumn(\"last_insert_rowid()\")"
//+"\n            return String(temp!).toInt()!"
//+"\n        }"
//+"\n        else {return -1}"
//+"\n  }";
//        
//        String syncinstring="";
//        for(int i=1;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            if(field.contentEquals("posted"))
//            {
//                syncinstring+="\n				item.posted=1";
//            }
//            else if(fieldtypes.get(i).contains("varchar") || (fieldtypes.get(i).contains("text") && !fieldtypes.get(i).contentEquals("text")) )
//            {
////syncinstring+="\n				if(attribs[\""+field+"\"] != nil){item.name=attribs[\""+field+"\"].replace(\"'\", \"''\")}";
//syncinstring+="\n				if let temp = (attribs[\""+field+"\"] as? "+datatypeFor(fieldtypes.get(i))+"){item."+field+"=temp.stringByReplacingOccurrencesOfString(\"'\", withString: \"''\")}";
//				
//
//            }
//            else
//            {
////syncinstring+="\n				if(attribs[\""+field+"\"] != nil){item.name=attribs[\""+field+"\"]}";
//syncinstring+="\n				if let temp = (attribs[\""+field+"\"] as? "+datatypeFor(fieldtypes.get(i))+"){item."+field+"=temp}";
//            }
//        }
//        
//        //function syncIn is only present if id is string
//        String syncinfuncstring="";
//if(fieldtypes.get(0).contains("varchar"))//no such thing as a text or longtext id
//{syncinfuncstring=
//"\n  class func syncIn(update: NSDictionary, errbank: ErrorBank)"
//+"\n  {"
//+"\n    switch(update[\"action\"] as String)"
//+"\n    {"
//+"\n      case \"create\":"
//+"\n        var attribs=update[\"new_or_updated_attributes\"] as NSDictionary"
//+"\n				var item=[tableCaps]()"
//+"\n				item."+fields.get(0)+"=update[\"entity_uuid\"] as String"
//+syncinstring
//+"\n				item.save()"
//+"\n        break;"
//+"\n      case \"update\":"
//+"\n        var attribs=update[\"new_or_updated_attributes\"] as NSDictionary"
//+"\n				if var item=[tableCapsPlural].getById(update[\"entity_uuid\"] as String)"
//+"\n				{"
//+syncinstring
//+"\n				item.save()"
//+"\n				}"
//+"\n        else"
//+"\n        {"
//+"\n          let log_num=update[\"log_num\"] as Int"
//+"\n          errbank.addError(\"Error processing update: Log Id \"+String(log_num)+\": record not found\")"
//+"\n        }"
//+"\n      case \"delete\":if var item=[tableCapsPlural].getById(update[\"entity_uuid\"] as "+datatypeFor(fieldtypes.get(0))+"){item.delete()};break;"
//+"\n      default: break;"
//+"\n    }"
//+"\n  }";
//}
        String attributesstring="";
        for(int i=0;i<fields.size();i++)
        {
            if(fieldtypes.get(i).contentEquals("date"))
                attributesstring+="\n            \""+fields.get(i)+"\": DateTimeHelper.toString(item."+fields.get(i)+"),";
            else
                attributesstring+="\n            \""+fields.get(i)+"\": item."+fields.get(i)+",";
        }
                
        String samplecreatestring="";
        for(int i=1;i<fields.size();i++)
        {
            String newuuid="3e944250----------------9992a9b07ea8";
//            if(fields.get(i).contains("_id"))
//            {
//datatypeInitialValueFor
//                if(fieldtypes.get(i).contentEquals("date"))
//                attributesstring+="\n            \""+fields.get(i)+"\": DateTimeHelper.toString(item."+fields.get(i)+"),";
//            else
                samplecreatestring+="\n    newitem."+fields.get(i)+"=\"\"";
//        }
//+"\n    newitem.employee_id=\"3e944250-cd06-4d93-ad1f-9992a9b07ea8\""
//+"\n    newitem.doctor_id=\"6b47d830-839f-416e-9506-5a3babb8b1c2\""
//+"\n    newitem.content=\"Boo2\""
//+"\n    newitem.date=NSDate()"                
        }
        String output="\n//"
+"\n//  SyncOut.swift"
+"\n//"
+"\n//  Generated by Jasper Tomas for 2014 intelimina.com. Open Source license applies"
+"\n//"
+"\n"
+"\nimport Foundation"
+"\n"
+"\nclass [tableCapsPlural]OutSyncHelper"
+"\n{"
+"\n  struct Static"
+"\n  {"
+"\n    //set url here"
+"\n    static let url=Constants.UPDATE_API_ENDPOINT_URL"
+"\n    static let method=\"POST\""
+"\n  }"
+"\n  "
+"\n  //this is the main function"
+"\n  class func sync(view: ViewController2)"
+"\n  {"
+"\n    //check database if there are [tablePlural] to sync out"
+"\n    let syncableitems=[tableCapsPlural].select(\" where posted='0'\")"
+"\n    //if no items to sync out, do nothing"
+"\n    if(syncableitems.count==0)"
+"\n    {"
+"\n      self.guiCloseAction(view)"
+"\n      self.dealWithError(\"nothingtopost\")"
+"\n      return"
+"\n    }"
+"\n    "
+"\n    //put together parameter data here"
+"\n    var attributesarray: [Dictionary<String, AnyObject>]=[]"
+"\n    var attributes: Dictionary<String, AnyObject>=[:]"
+"\n    for item in syncableitems"
+"\n    {"
+"\n      if let user=Users.getById(item.employee_id)"
+"\n      {"
+"\n        attributes ="
+"\n          ["
+attributesstring                
+"\n          ]"
+"\n        attributesarray.append(attributes)"
+"\n      }"
+"\n    }"
+"\n    let params : Dictionary<String, AnyObject>=["
+"\n      \"auth_token\":Global.strings[\"auth_token\"]!,"
+"\n      \"action\":\"create\","
+"\n      \"entity\":\"[tableCaps]\","
+"\n      \"new_or_updated_attributes\":attributesarray"
+"\n    ]"
+"\n    //println(params)"
+"\n    //post it"
+"\n    post(params,view: view)"
+"\n  }"
+"\n  //define here: what GUI does after process ends,"
+"\n  //whether success or failure"
+"\n  private class func guiCloseAction(view: ViewController2)"
+"\n  {"
+"\n    dispatch_async(dispatch_get_main_queue(),{"
+"\n      view.stopSpinnerOutgoingSync()"
+"\n    });"
+"\n  }"
+"\n  //process response data here (request is a success)"
+"\n  private class func processJSON(parseJSON: NSDictionary)"
+"\n  {"
+"\n    println(parseJSON)"
+"\n    var entity_uuid=\"\""
+"\n    //for each item with success = 1, set posted=true and save"
+"\n    var items=parseJSON[\"results\"] as [NSDictionary]"
+"\n    for item in items"
+"\n    {"
+"\n      entity_uuid=item[\"entity_uuid\"] as String"
+"\n      if(item[\"success\"] as Int == 1)"
+"\n      {"
+"\n        if let item = [tableCapsPlural].getById(entity_uuid)"
+"\n        {"
+"\n          item.posted=1"
+"\n          item.save()"
+"\n        }"
+"\n      }"
+"\n    }"
+"\n  }"
+"\n  //define here: how to deal with errors (request is a failure)"
+"\n  private class func dealWithError(errorstring: String)"
+"\n  {"
+"\n    switch(errorstring)"
+"\n    {"
+"\n    case \"nothingtopost\":"
+"\n      dispatch_async(dispatch_get_main_queue(),{"
+"\n        UIHelper.popupAndWriteToLog(\"No [tablePlural] need uploading\")"
+"\n      });"
+"\n    case \"servernotfound\":"
+"\n      dispatch_async(dispatch_get_main_queue(),{"
+"\n        UIHelper.popupAndWriteToLog(\"Server not found\")"
+"\n      });"
+"\n      break;"
+"\n    case \"nointernetaccess\":"
+"\n      dispatch_async(dispatch_get_main_queue(),{"
+"\n        UIHelper.popupAndWriteToLog(\"Please check your internet connection\")"
+"\n      });"
+"\n      break;"
+"\n    case \"invalid\":"
+"\n      dispatch_async(dispatch_get_main_queue(),{"
+"\n        UIHelper.popupAndWriteToLog(\"Invalid username or password\")"
+"\n      });"
+"\n      break;"
+"\n    default:"
+"\n      dispatch_async(dispatch_get_main_queue(),{"
+"\n        UIHelper.popupAndWriteToLog(\"Error posting [tablePlural]: \"+errorstring)"
+"\n      });"
+"\n      break;"
+"\n    }"
+"\n  }"
+"\n  //no customizable sections here"
+"\n  private class func post(params: Dictionary<String, AnyObject>,view: ViewController2) {"
+"\n    "
+"\n    var request = NSMutableURLRequest(URL: NSURL(string: Static.url)!)"
+"\n    var session = NSURLSession.sharedSession()"
+"\n    request.HTTPMethod = Static.method"
+"\n    "
+"\n    var err: NSError?"
+"\n    request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)"
+"\n    request.addValue(\"application/json\", forHTTPHeaderField: \"Content-Type\")"
+"\n    request.addValue(\"application/json\", forHTTPHeaderField: \"Accept\")"
+"\n    "
+"\n    var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in"
+"\n      print(\"data:\");println(data)"
+"\n      print(\"response:\");println(response)"
+"\n      print(\"error:\");println(error)"
+"\n      "
+"\n      //otherwise if there's an error"
+"\n      if(error != nil)"
+"\n      {"
+"\n        switch error.code"
+"\n        {"
+"\n        case -1009: self.dealWithError(\"nointernetaccess\"); break;"
+"\n        case -1003: self.dealWithError(\"servernotfound\"); break;"
+"\n        default: self.dealWithError(error.localizedDescription); break;"
+"\n        }"
+"\n        self.guiCloseAction(view)"
+"\n        return"
+"\n      }"
+"\n      "
+"\n      //if status code is not 200 (success)"
+"\n      let statuscode=String((response as NSHTTPURLResponse).statusCode)"
+"\n      if(statuscode != \"200\")"
+"\n      {"
+"\n        switch statuscode"
+"\n        {"
+"\n        case \"401\":self.dealWithError(\"invalid\");break;"
+"\n        case \"404\":self.dealWithError(\"servernotfound\");break;"
+"\n        default: self.dealWithError(\"HTTP Error Code: \\(statuscode)\");break;"
+"\n        }"
+"\n        self.guiCloseAction(view)"
+"\n        return"
+"\n      }"
+"\n      "
+"\n      "
+"\n      //http request success ok"
+"\n      //retrieve data"
+"\n      var strData = NSString(data: data, encoding: NSUTF8StringEncoding)"
+"\n      //      println(\"~Body: \\(strData)\")"
+"\n      var err: NSError?"
+"\n      "
+"\n      //convert to Json"
+"\n      var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary"
+"\n      "
+"\n      // Did the JSONObjectWithData constructor return an error? If so, log the error to the console"
+"\n      if(err != nil) {"
+"\n        println(\"~\"+err!.localizedDescription)"
+"\n        let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)"
+"\n        println(\"~Error could not parse JSON: '\\(jsonStr)'\")"
+"\n        println(err!.code)"
+"\n      }"
+"\n      else {"
+"\n        // The JSONObjectWithData constructor didn't return an error. But, we should still"
+"\n        // check and make sure that json has a value using optional binding."
+"\n        if let parseJSON = json {"
+"\n          self.processJSON(parseJSON)"
+"\n          self.guiCloseAction(view)"
+"\n        }"
+"\n        else {"
+"\n          // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?"
+"\n          let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)"
+"\n          println(\"Error could not parse JSON: \\(jsonStr)\")"
+"\n        }"
+"\n      }"
+"\n      self.guiCloseAction(view)"
+"\n    })"
+"\n    "
+"\n    task.resume()"
+"\n  }"
+"\n}"
+"\n/*"
+"\nURL: http://biofemme-dev.intelimina.com/api/v1/updates.json"
+"\n"
+"\nsample json data sent"
+"\n[action: create, auth_token: vW35ZBFZ2nNjxuiYia6G, new_or_updated_attributes: ("
+"\n        {"
+"\n        body = \"Hi there\";"
+"\n        \"business_unit_ids\" =         ("
+"\n            \"6ed9f543-96ba-43f0-9f0d-274d275b6796\""
+"\n        );"
+"\n        \"creator_id\" = \"3e944250-cd06-4d93-ad1f-9992a9b07ea8\";"
+"\n        \"creator_type\" = User;"
+"\n        \"date_created\" = \"2014-01-01 00:00:00\";"
+"\n        \"doctor_id\" = \"6b47d830-839f-416e-9506-5a3babb8b1c2\";"
+"\n        id = \"1112C2EA-5102-4778-A99D-A3C668482B36\";"
+"\n    },"
+"\n        {"
+"\n        body = Boo2;"
+"\n        \"business_unit_ids\" =         ("
+"\n            \"6ed9f543-96ba-43f0-9f0d-274d275b6796\""
+"\n        );"
+"\n        \"creator_id\" = \"3e944250-cd06-4d93-ad1f-9992a9b07ea8\";"
+"\n        \"creator_type\" = User;"
+"\n        \"date_created\" = \"2014-12-01 00:00:00\";"
+"\n        \"doctor_id\" = \"6b47d830-839f-416e-9506-5a3babb8b1c2\";"
+"\n        id = \"AB4EDFC5-485E-40D2-9695-CEF20A9C841C\";"
+"\n    }"
+"\n), entity: [tableCaps]]"
+"\n"
+"\n"
+"\ncode to create dummy transmission"
+"\nvar business_units: [String]=[\"6ed9f543-96ba-43f0-9f0d-274d275b6796\"]"
+"\n["
+"\n\"id\": UuidGenerator.generate(),"
+"\n\"creator_id\": \"3e944250-cd06-4d93-ad1f-9992a9b07ea8\","
+"\n\"creator_type\": \"User\","
+"\n\"doctor_id\": \"6b47d830-839f-416e-9506-5a3babb8b1c2\","
+"\n\"body\":\"[tableCaps] here.\","
+"\n\"date_created\":\"2013-10-25 11:45:41\","
+"\n\"business_unit_ids\":business_units"
+"\n]"
+"\nor you can do this:"
+"\n    var newitem=[tableCaps]()"
+"\n    newitem.id=UuidGenerator.generate()"
+samplecreatestring
+"\n    newitem.save()"
+"\n"
+"\n"
+"\nresult json:"
+"\n{"
+"\nresults =     ("
+"\n{"
+"\n\"entity_uuid\" = \"B16D970E-C0EA-45B7-BD5E-1B836A224D4B\";"
+"\nsuccess = 1;"
+"\n}"
+"\n);"
+"\nsuccess = 1;"
+"\n}"
+"\n"
+"\n*/"
;
                
                
//        String output="package models;"
//+"\n"
//+"\nimport java.io.IOException;"
//+"\nimport java.math.BigDecimal;"
//+"\nimport java.sql.Connection;"
//+"\nimport java.sql.ResultSet;"
//+"\nimport java.sql.SQLException;"
//+"\nimport java.sql.Statement;"
//+"\nimport java.sql.Date;"
//+"\nimport java.sql.Timestamp;"
//+"\nimport java.util.ArrayList;"
//+"\nimport java.util.logging.Level;"
//+"\nimport java.util.logging.Logger;"
//+"\nimport utils.MySqlDBHelper;"
//+"\nimport utils.JsonHelper;"
//+"\n"
//+"\npublic class [tableCapsPlural] {"
//+"\n    //------------FIELDS-----------"
//+"\n    public static final String tablename=[tableCaps].tablename;"
//+"\n    public static String[] fields=[tableCaps].fields;"
//+"\n    public static String[] fieldtypes=[tableCaps].fieldtypes;"
//+"\n    //-----------------------"
//+"\n    //-------------------------TABLE FUNCTIONS---------------------"
//+"\n"
//+"\n    //-----------getter functions----------"
//+"\n    /*"
//+"\n    public static [tableCapsPlural] getByName(String name)"
//+"\n    {"
//+"\n            HashMap<"+iddatatype+",[tableCapsPlural]> map=select(\" name = '\"+name+\"'\");"
//+"\n            for([tableCapsPlural] item:map)return item;"
//+"\n            return null;"
//+"\n    }	"
//+"\n    */"
//+"\n    public static [tableCaps] getBy"+toCamelCase(idfield)+"("+iddatatype+" "+idfield+") {"
//+"\n            RecordList map=select(\" "+idfield+" = '\"+"+idfield+idfieldtypestringifier+"+\"'\");"
//+"\n            for([tableCaps] item:map)return item;"
//+"\n            return null;"
//+"\n    }"
//+"\n    //-----------database functions--------------"
//+"\n"
//+"\n    public static void delete("+iddatatype+" "+idfield+")"
//+"\n    {"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();            "
//+"\n        Statement st = null;"
//+"\n        try { "
//+"\n            st = conn.createStatement();"
//+"\n            st.executeUpdate(\"delete from \"+tablename+\" where "+idfield+" = '\"+"+idfield+idfieldtypestringifier+"+\"';\");"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n        }"
//+"\n    }"
//+"\n    public static void delete([tableCaps] item)"
//+"\n    {"
//+"\n        delete(item.get"+toCamelCase(idfield)+"());"
//+"\n    }"
//+"\n    public static void insert([tableCaps] item)"
//+"\n    {"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();            "
//+"\n        Statement st = null;"
//+"\n        boolean withid=false;"
//+"\n        try { "
//+"\n            st = conn.createStatement();"
//+"\n            //for tables with integer primary key"
//+"\n            if(fieldtypes[0].contentEquals(\"integer\"))withid=false;                "
//+"\n            //for tables with varchar primary key"
//+"\n            else if(fieldtypes[0].contains(\"varchar\"))withid=true;                "
//+"\n            st.executeUpdate(\"INSERT INTO \"+tablename+\" (\"+implodeFields(withid)+\")VALUES (\"+implodeValues(item, withid)+\");\");"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n        }"
//+"\n    }"
//+"\n    public static void update([tableCaps] item)"
//+"\n    {"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();            "
//+"\n        Statement st = null;"
//+"\n        boolean withid=false;"
//+"\n        try { "
//+"\n            st = conn.createStatement();"
//+"\n            st.executeUpdate(\"update \"+tablename+\" set \"+implodeFieldsWithValues(item,false)+\" where "+idfield+" = '\"+item.get"+toCamelCase(idfield)+"()"+idfieldtypestringifier+"+\"';\");"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n        }"
//+"\n    }"
//+"\n    public static Integer count(String conditions)"
//+"\n    {"
//+"\n        if(conditions.isEmpty())conditions = \"1\";"
//+"\n"
//+"\n        //if conditions contains a limit clause, remove it. "
//+"\n        //It is not applicable to a count query"
//+"\n        else if(conditions.contains(\"limit\"))"
//+"\n        {"
//+"\n            String[] segments=conditions.split(\"limit\");"
//+"\n            conditions=segments[0];"
//+"\n        }"
//+"\n"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();"
//+"\n        Statement st = null;"
//+"\n        ResultSet rs = null;"
//+"\n        try { "
//+"\n        st = conn.createStatement();"
//+"\n        rs = st.executeQuery(\"SELECT count(*) from \"+tablename+\" where \"+conditions);"
//+"\n            while (rs.next()) {"
//+"\n                return rs.getInt(1);"
//+"\n            }"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n        }"
//+"\n        return null;"
//+"\n    }"
//+"\n    public static RecordList select(String conditions)"
//+"\n    {"
//+"\n        if(conditions.isEmpty())conditions = \"1\";"
//+"\n        Connection conn=MySqlDBHelper.getInstance().getConnection();"
//+"\n        Statement st = null;"
//+"\n        ResultSet rs = null;"
//+"\n        try { "
//+"\n            st = conn.createStatement();"
//+"\n                rs = st.executeQuery(\"SELECT * from \"+tablename+\" where \"+conditions);"
//+"\n"
//+"\n            RecordList items=new RecordList();"
//+"\n            while (rs.next()) {"
//+"\n                items.add(new [tableCaps](rs));"
//+"\n                    //items.put(rs."+rsGetterFor(idfieldtype)+"(\""+idfield+"\"), new [tableCapsPlural](rs));"
//+"\n            }"
//+"\n            return items;"
//+"\n        } catch (SQLException ex) {"
//+"\n            Logger.getLogger([tableCapsPlural].class.getName()).log(Level.SEVERE, null, ex);"
//+"\n            ex.printStackTrace();"
//+"\n            return null;"
//+"\n        }"
//+"\n    }"
//+"\n"
//+"\n    //-----------database helper functions--------------"
//+"\n    public static String implodeValues([tableCaps] item,boolean withId)"
//+"\n    {"
//+"\n            ArrayList<String> values=item.implodeFieldValuesHelper(withId);"
//+"\n            String output=\"\";"
//+"\n            for(String value:values)"
//+"\n            {"
//+"\n                    if(!output.isEmpty())"
//+"\n                            output+=\",\";"
//+"\n                    output+=(value!=null?\"'\"+value+\"'\":\"null\");"
//+"\n            }"
//+"\n            return output;"
//+"\n    }"
//+"\n    public static String implodeFields(boolean withId)"
//+"\n    {"
//+"\n            String output=\"\";"
//+"\n            for(String field:fields)"
//+"\n            {"
//+"\n                    if(!withId && field.contentEquals(\""+idfield+"\"))continue;"
//+"\n                    if(!output.isEmpty())"
//+"\n                            output+=\",\";"
//+"\n                    output+=field;"
//+"\n            }"
//+"\n            return output;"
//+"\n    }"
//+"\n    public static String implodeFieldsWithValues([tableCaps] item,boolean withId)"
//+"\n    {"
//+"\n            ArrayList<String> values=item.implodeFieldValuesHelper(true);//get entire list of values; whether the id is included will be dealt with later."
//+"\n"
//+"\n            if(values.size()!=fields.length)"
//+"\n            {"
//+"\n                    System.err.println(\"[tableCapsPlural]:implodeFieldsWithValues(): ERROR: values length does not match fields length\");"
//+"\n            }"
//+"\n"
//+"\n            String output=\"\";"
//+"\n            for(int i=0;i<fields.length;i++)"
//+"\n            {"
//+"\n                    if(!withId && fields[i].contentEquals(\""+idfield+"\"))continue;"
//+"\n                    if(!output.isEmpty())"
//+"\n                            output+=\",\";"
//+"\n                    output+=fields[i]+\"=\"+(values.get(i)!=null?\"'\"+values.get(i)+\"'\":\"null\");"
//+"\n            }"
//+"\n            return output;"
//+"\n    }	"
//+"\n    public static String implodeFieldsWithTypes()"
//+"\n    {"
//+"\n            String output=\"\";"
//+"\n            for(int i=0;i<fields.length;i++)"
//+"\n            {"
//+"\n                    if(fields[i].contentEquals(fields[0]))//fields[0] being the primary key"
//+"\n                            output+=fields[i]+\" \"+fieldtypes[i]+\" PRIMARY KEY\";"
//+"\n                    else"
//+"\n                            output+=\",\"+fields[i]+\" \"+fieldtypes[i];"
//+"\n            }"
//+"\n            return output;"
//+"\n    }	"
//+"\n    public static String createTable()"
//+"\n    {"
//+"\n            return \"CREATE TABLE IF NOT EXISTS \"+tablename+\" (\"+implodeFieldsWithTypes()+\" );\";"
//+"\n    }"
//+"\n    public static String deleteTable()"
//+"\n    {"
//+"\n            return \"DROP TABLE IF EXISTS \"+tablename;"
//+"\n    }"
//+"\n    public static class RecordList extends ArrayList<[tableCaps]>{"
//+"\n        public static RecordList fromJsonString(String resultstring) throws IOException"
//+"\n        {"
//+"\n            return JsonHelper.mapper.readValue(resultstring, RecordList.class);"
//+"\n        }"
//+"\n        public String toEscapedJsonString() throws IOException"
//+"\n        {"
//+"\n            return \"\\\"\"+JsonHelper.mapper.writeValueAsString(this).replace(\"\\\"\", \"\\\\\\\"\") +\"\\\"\";"
//+"\n        }"
//+"\n    }"
//+"\n    public static void main(String args[])"
//+"\n    {"
//+"\n        String database=\""+database+"\";"
//+"\n        String url = \"jdbc:mysql://"+hostname+":3306/\"+database+\"?zeroDateTimeBehavior=convertToNull\";"
//+"\n        String username=\""+username+"\";"
//+"\n        String password = \""+password+"\";"
//+"\n"
//+"\n        boolean result=MySqlDBHelper.init(url, username, password);            "
//+"\n"
//+"\n        RecordList items=[tableCapsPlural].select(\"\");"
//+"\n        for([tableCaps] item:items)"
//+"\n        {"
//+"\n            System.out.println(item);"
//+"\n        }"
//+"\n        System.out.println([tableCapsPlural].count(\"\"));"
//+"\n    } "
//+"\n}"
//+"\n";
        String tableplural=table;
        String tablesingular=singularize(table);
        String tablecapssingular=toCamelCase(tablesingular);
        String tablecapsplural=toCamelCase(tableplural);
        output=output.replace("[tableCaps]", tablecapssingular);
        output=output.replace("[table]", table);
        output=output.replace("[tableCapsPlural]", tablecapsplural);
        output=output.replace("[tablePlural]", tableplural);

        return output;
    }
//    public static void createModelFile(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
//    {
//        //write file to current directory
//        File outputdir=new File("./src/models");
//        outputdir.mkdir();
//        FileWriter.write(outputdir.getPath()+"/"+toCamelCase(singularize(table))+".swift", createModelFileContents(table,fields,fieldtypes));
//    }
//    public static String createModelFileContents(String table, ArrayList<String> fields, ArrayList<String> fieldtypes)
//    {
//        int counter=0;
//        
////        String fieldsstring="";
////        for(int i=0;i<fields.size();i++)
////        {
////            fieldsstring+="\n            "+(i==0?"":",")+"\""+fields.get(i) +"\"";
////        }
////        
////        String fieldtypesstring="";
////        for(int i=0;i<fieldtypes.size();i++)
////        {
////            fieldtypesstring+="\n            "+(i==0?"":",")+"\""+fieldtypes.get(i) +"\"";
////        }
////        
//        String idfield=fields.get(0);
//        String idfieldtype=fieldtypes.get(0);
//        String idfieldtypestringifier=stringifier(fieldtypes.get(0));
//        String iddatatype=datatypeFor(fieldtypes.get(0));
//        
//        String gettersandsetters="";
//        String datatype="";
//        String field="";
//        String fieldtype="";
////        for(int i=0;i<fields.size();i++)
////        {
////            field=fields.get(i);
////            datatype=datatypeFor(fieldtypes.get(i));
////            gettersandsetters+=
////"\n    public "+datatype+" get"+toCamelCase(field)+"() {"
////+"\n            return "+field+";"
////+"\n    }"
////+"\n"
////+"\n    public void set"+toCamelCase(field)+"("+datatype+" "+field+") {"
////+"\n            this."+field+" = "+field+";"
////+"\n    }"
////+"\n";
////        }
////        
///*
//    var id: Int = 0
//    var studentRollNo: String = String()
//    var studentName: String = String()  
//*/
//        String vardefinitions="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            datatype=datatypeFor(fieldtypes.get(i));
//            vardefinitions+=
//"\n    var "+field+": "+datatype+" = "+datatypeInitialValueFor(fieldtypes.get(i));
//        }
//                        
///*
//    self.id=rsMain.intForColumn("id")
//    if let temp = rsMain.stringForColumn("student_rollno")
//    {
//      self.studentRollNo=temp
//    }
//    if let temp = rsMain.stringForColumn("student_name")
//    {
//      self.studentName=temp
//    }
// */
//        String constructorfields="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            if(fieldtypes.get(i).contains("varchar"))
//            {
//                constructorfields+="\n    if let temp = rsMain."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\"){self."+field+"=temp}";
//            }
//            else if(fieldtypes.get(i).contentEquals("date"))
//            {
//                constructorfields+="\n    if let temp = rsMain.stringForColumn(\""+field+"\"){self."+field+"=DateHelper.toDate(temp)}";
//            }
//            else if(fieldtypes.get(i).contentEquals("text"))
//            {
//                constructorfields+="\n    if let temp = rsMain."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\"){"+field+"=DatabaseHelper.parseCommaSeparatedString(temp)}";
//            }
//            else if(fieldtypes.get(i).contains("int")&&!fieldtypes.get(i).contains("tiny")&&!fieldtypes.get(i).contains("small"))
//            {
//                constructorfields+="\n    self."+field+" = String(rsMain."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\")).toInt()!";
//            }
//            else
//            {
//                constructorfields+="\n    self."+field+" = rsMain."+rsGetterFor(fieldtypes.get(i))+"(\""+field+"\")";
//            }
//        }
// 
///*
//    if includeId {string+="'"+String(id)+"',"}
//    string+="'"+studentRollNo+"'"
//    string+=","+"'"+studentName+"'"
//  */
//        String implodevaluesstring="";
//        String comma="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            fieldtype=fieldtypes.get(i);
//            if(i==0)
//            {
//                implodevaluesstring+=
//"\n    if includeId {"
//+"\n        string+=\"'\"+"+stringifiedWithNull(field,fieldtype)+"+\"',\""
//+"\n    }";
////"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
//            }
//            else
//            {
//                if(i==1)comma="";else comma=",";
//                implodevaluesstring+=
//"\n    string+=\""+comma+"'\"+"+stringifiedWithNull(field,fieldtype)+"+\"'\"";
////"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
//            }
//        }
//
//        String implodefieldsandvaluesstring="";
//        for(int i=0;i<fields.size();i++)
//        {
//            field=fields.get(i);
//            fieldtype=fieldtypes.get(i);
//            if(i==0)
//            {
//                implodefieldsandvaluesstring+=
//"\n    if includeId {"
//+"\n        string+=\""+field+"='\"+"+stringifiedWithNull(field,fieldtype)+"+\"',\""
//+"\n    }";
////"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
//            }
//            else
//            {
//                if(i==1)comma="";else comma=",";
//                implodefieldsandvaluesstring+=
//"\n    string+=\""+comma+field+"='\"+"+stringifiedWithNull(field,fieldtype)+"+\"'\"";
////"\n            "+(field.contentEquals(idfield)?"if(withId)":"")+"values.add("+stringifiedWithNull(field,fieldtype)+");";
//            }
//        }
//        
//        String savestring="";
//        if(iddatatype.contentEquals("String"))
//            savestring=
//            "\n    if("+idfield+"==\"\"){id=UuidGenerator.generate()}"
//            +"\n    if(!_saved)"
//            +"\n    {"
//            +"\n      _saved=true"
//            +"\n      return [tableCapsPlural].insert(self)"
//            +"\n    }";
//        else
//            savestring=
//            "\n    if(!_saved)"
//            +"\n    {"
//            +"\n      _saved=true"
//            +"\n      let result=[tableCapsPlural].insert(self)"
//            +"\n      if(result){id=[tableCapsPlural].getLastInsertId()}"
//            +"\n      return result"
//            +"\n    }"
//                    ;
////        if(iddatatype.contentEquals("String"))
////            savestring="\n            if("+idfield+"==null || "+idfield+".isEmpty() )";
////        else
////            savestring="\n            if("+idfield+"==null || "+idfield+"==0)";
//        
//        String output="import UIKit"
//+"\n"
//+"\nclass [tableCaps] {"
//+vardefinitions
//+"\n    var _saved: Bool = false  //saved to database or not"
//+"\n  "
//+"\n  //constructors"
//+"\n  init(){}"
//+"\n  init(rsMain: FMResultSet)"
//+"\n  {"
//+"\n    //obtained from database: saved is true"
//+"\n    _saved=true"
//+constructorfields
//+"\n  }"
//+"\n"
//+"\n  //helpers"
//+"\n  func commaSeparatedValues(includeId: Bool = true)->String"
//+"\n  {"
//+"\n    var string=\"\""
//+"\n"
//+implodevaluesstring
//+"\n    "
//+"\n    return string"
//+"\n  }"
//+"\n  func commaSeparatedFieldsAndValues(includeId: Bool = true)->String"
//+"\n  {"
//+"\n    var string=\"\""
//+"\n"
//+implodefieldsandvaluesstring
//+"\n    "
//+"\n    return string"
//+"\n  }"
//+"\n"
//+"\n"
//+"\n  func delete()->Bool"
//+"\n  {"
//+"\n    return [tableCapsPlural].delete(self)"
//+"\n  }"
//+"\n  func save()->Bool"
//+"\n  {"
//+savestring
//+"\n    else"
//+"\n    {"
//+"\n      return [tableCapsPlural].update(self)"
//+"\n    }"
//+"\n  }"
//+"\n}";
//
//        String tableplural=table;
//        String tablesingular=singularize(table);
//        String tablecapssingular=toCamelCase(tablesingular);
//        String tablecapsplural=toCamelCase(tableplural);
//        output=output.replace("[tableCaps]", tablecapssingular);
//        output=output.replace("[table]", table);
//        output=output.replace("[tableCapsPlural]", tablecapsplural);
//        output=output.replace("[tablePlural]", tableplural);
//        
//        return output;
//    }
    private static String singularize(String table)
    {
        String singular=table;
        if(table.substring(table.length()-3, table.length()).contentEquals("ies"))
        {
            singular=table.substring(0,table.length()-3)+"y";
        }
        else singular=table.substring(0,table.length()-1);
        return singular;
    }
    
    //-----utils-----
    public static String toCamelCase(String string)
    {
        String[] segments=string.split("_");
        String output="";
        for(String s:segments)
            output+=capitalize(s);
        return output;
    }
    public static String capitalize(String s)
    {
        return s.substring(0, 1).toUpperCase()+s.substring(1);
    }
    public static String datatypeFor(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        //System.out.println(type);
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "Int";
        else if(type.contentEquals("varchar()"))
            return "String";
        else if(type.contentEquals("char()"))
            return "String";
        else if(type.contentEquals("text"))
            return "[String]";
        else if(type.contentEquals("tinytext"))
            return "String";
        else if(type.contentEquals("mediumtext"))
            return "String";
        else if(type.contentEquals("longtext"))
            return "String";
        else if(type.contentEquals("date"))
            return "NSDate";
        else if(type.contains("bigint"))
            return "Long";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "Bool";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "BigDecimal";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "Float";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "Double";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "Boolean";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "Timestamp";
        else if(type.contains("enum"))
            return "String";
        else 
            return "";
/*
<option value="INT" selected="selected">INT</option>
<option value="VARCHAR">VARCHAR</option>
<option value="TEXT">TEXT</option>
<option value="DATE">DATE</option>
<optgroup label="NUMERIC"><option value="TINYINT">TINYINT</option>
<option value="SMALLINT">SMALLINT</option>
<option value="MEDIUMINT">MEDIUMINT</option>
<option value="INT" selected="selected">INT</option>
<option value="BIGINT">BIGINT</option>
<option value="-">-</option>
<option value="DECIMAL">DECIMAL</option>
<option value="FLOAT">FLOAT</option>
<option value="DOUBLE">DOUBLE</option>
<option value="REAL">REAL</option>
<option value="-">-</option>
<option value="BIT">BIT</option>
<option value="BOOLEAN">BOOLEAN</option>
<option value="SERIAL">SERIAL</option>
</optgroup><optgroup label="DATE and TIME"><option value="DATE">DATE</option>
<option value="DATETIME">DATETIME</option>
<option value="TIMESTAMP">TIMESTAMP</option>
<option value="TIME">TIME</option>
<option value="YEAR">YEAR</option>
</optgroup><optgroup label="STRING"><option value="CHAR">CHAR</option>
<option value="VARCHAR">VARCHAR</option>
<option value="-">-</option>
<option value="TINYTEXT">TINYTEXT</option>
<option value="TEXT">TEXT</option>
<option value="MEDIUMTEXT">MEDIUMTEXT</option>
<option value="LONGTEXT">LONGTEXT</option>
<option value="-">-</option>
<option value="BINARY">BINARY</option>
<option value="VARBINARY">VARBINARY</option>
<option value="-">-</option>
<option value="TINYBLOB">TINYBLOB</option>
<option value="MEDIUMBLOB">MEDIUMBLOB</option>
<option value="BLOB">BLOB</option>
<option value="LONGBLOB">LONGBLOB</option>
<option value="-">-</option>
<option value="ENUM">ENUM</option>
<option value="SET">SET</option>
</optgroup><optgroup label="SPATIAL"><option value="GEOMETRY">GEOMETRY</option>
<option value="POINT">POINT</option>
<option value="LINESTRING">LINESTRING</option>
<option value="POLYGON">POLYGON</option>
<option value="MULTIPOINT">MULTIPOINT</option>
<option value="MULTILINESTRING">MULTILINESTRING</option>
<option value="MULTIPOLYGON">MULTIPOLYGON</option>
<option value="GEOMETRYCOLLECTION">GEOMETRYCOLLECTION</option>
</optgroup>    
 */        
    }
    public static String datatypeInitialValueFor(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        //System.out.println(type);
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "0";
        else if(type.contentEquals("varchar()"))
            return "\"\"";
        else if(type.contentEquals("char()"))
            return "\"\"";
        else if(type.contentEquals("text"))
            return "[]";
        else if(type.contentEquals("tinytext"))
            return "\"\"";
        else if(type.contentEquals("mediumtext"))
            return "\"\"";
        else if(type.contentEquals("longtext"))
            return "\"\"";
        else if(type.contentEquals("date"))
            return "DateHelper.getEmptyDate()";
        else if(type.contains("bigint"))
            return "0";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "false";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "0";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "0";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "0";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "false";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "Timestamp";
        else if(type.contains("enum"))
            return "String";
        else 
            return "";
    }
    public static String rsGetterFor(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "intForColumn";
        else if(type.contains("varchar()"))
            return "stringForColumn";
        else if(type.contentEquals("char()"))
            return "stringForColumn";
        else if(type.contentEquals("text"))
            return "stringForColumn";
        else if(type.contentEquals("tinytext"))
            return "stringForColumn";
        else if(type.contentEquals("mediumtext"))
            return "stringForColumn";
        else if(type.contentEquals("longtext"))
            return "stringForColumn";
        else if(type.contentEquals("date"))
            return "getDate";
        else if(type.contains("bigint"))
            return "getLong";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "boolForColumn";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "getBigDecimal";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "getFloat";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "getDouble";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "getBoolean";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "getTimestamp";
        else if(type.contains("enum"))
            return "getString";
        else 
            return "";  
    }    
    public static String stringifier(String type)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return ".toString()";
        else if(type.contains("varchar()"))
            return "";
        else if(type.contentEquals("char()"))
            return "";
        else if(type.contentEquals("text"))
            return "";
        else if(type.contentEquals("tinytext"))
            return "";
        else if(type.contentEquals("mediumtext"))
            return "";
        else if(type.contentEquals("longtext"))
            return "";
        else if(type.contentEquals("date"))
            return ".toString()";
        else if(type.contains("bigint"))
            return ".toString()";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return ".toString()";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return ".toString()";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return ".toString()";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return ".toString()";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return ".toString()";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return ".toString()";
        else if(type.contains("enum"))
            return "";
        else 
            return "";  
    }       
    public static String stringifiedWithNull(String field,String type)
    {
        type=type.replaceAll("[,0-9]", "");
        if(type.contentEquals("int")||type.contentEquals("int()"))
            return "String("+field+")";
        else if(type.contains("varchar()"))
            return field;
        else if(type.contentEquals("char()"))
            return field;
        else if(type.contentEquals("text"))
            return "DatabaseHelper.mergeCommaSeparatedString("+field+")";
        else if(type.contentEquals("tinytext"))
            return field;
        else if(type.contentEquals("mediumtext"))
            return field;
        else if(type.contentEquals("longtext"))
            return field;
        else if(type.contentEquals("date"))
            return "DateHelper.toString(date)";
        else if(type.contains("bigint"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contains("tinyint") || type.contains("smallint") || type.contains("mediumint"))
            return "("+field+" ? \"1\" : \"0\")";
        else if(type.contentEquals("decimal")||type.contentEquals("decimal()"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contentEquals("float")||type.contentEquals("float()"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contentEquals("double")||type.contentEquals("double()"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contentEquals("boolean")||type.contentEquals("boolean()"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contentEquals("datetime")||type.contentEquals("timestamp"))
            return "("+field+" != nil ? String("+field+") : nil)";
        else if(type.contains("enum"))
            return field;
        else 
            return field;  
    }    

}
