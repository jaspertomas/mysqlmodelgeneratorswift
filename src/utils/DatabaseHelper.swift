//
//  DatabaseHelper.swift
//  LoginExample
//
//  Created by Jasper Tomas on 11/10/14.
//  Copyright (c) 2014 intelimina.com. All rights reserved.
//

import Foundation

class DatabaseHelper
{
  struct Instance
  {
    private static var instance: FMDatabase?=nil
    private static var initialized=false
    private static var dbFilePath:String=""
  }

  private init(){}
  private class func initialize()->Bool
  {
//  println("DatabaseHelper.initialize()")
        //make sure this code runs only once
        if(Instance.initialized)
        {
          return true
        }
  
        //formulate db file path
        let documentFolderPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let dbfile = "/" + Constants.DATABASE_FILE_NAME;
        Instance.dbFilePath = documentFolderPath.stringByAppendingString(dbfile)
    
        //initialize filemanager
//        println("initializing db")
        let filemanager = NSFileManager.defaultManager()
        //if necessary, copy database to Documents folder
        if (!filemanager.fileExistsAtPath(Instance.dbFilePath) ) {
 
            let backupDbPath = NSBundle.mainBundle().pathForResource(Constants.DATABASE_RESOURCE_NAME, ofType: Constants.DATABASE_RESOURCE_TYPE)
 
            if (backupDbPath == nil) {
                //initialization failed
                println("Database initialization failed")
                return false
            } else {
                var error: NSError?
              
                //copy database to Documents folder
                let copySuccessful = filemanager.copyItemAtPath(backupDbPath!, toPath:Instance.dbFilePath, error: &error)
              
                //if copy failed
                if !copySuccessful {
                    //initialization failed
                    //print error message to log
                    println("Database copy failed: \(error?.localizedDescription)")
                    return false
                }
            }
        }
        return true
  }
  
  class func getInstance()->FMDatabase?
  {
    //if instance is not initialized
    if(!Instance.initialized)
    {
      //initialize instance
      Instance.initialized=initialize()
      //if initialization failed
      if(!Instance.initialized)
      {
        //return nil
        return nil
      }
    }
    //either instance already initialized
    //or initialization successful
    
    // initialize FMDB
    if(Instance.instance == nil)
    {
      Instance.instance = FMDatabase(path:Instance.dbFilePath)
    }
    if(Instance.instance == nil)
    {
      println("Error instantiating db")
      return nil
    }
    else if (Instance.instance!.open() == false) {
        NSLog("error opening db")
        return nil
    }
    else
    {
      //return instance
      return Instance.instance!
    }
    
  }
  class func parseCommaSeparatedString(string: String)-> [String]
  {
    var array:[String]=[]
    var newstring = string
    newstring = newstring.stringByReplacingOccurrencesOfString("[\"", withString: "")
    newstring = newstring.stringByReplacingOccurrencesOfString("\"]", withString: "")
    return newstring.componentsSeparatedByString("\",\"")
  }
  class func mergeCommaSeparatedString(array: [String])-> String
  {
    var newstring=""
    var counter=0
    for string in array{
      if(counter==0){newstring="[\"\(string)\""}
      else {newstring+=",\"\(string)\""}
      counter++
    }
    newstring+="]"
    return newstring
  }

//  class func listtables()
//  {
//        //list tables
//        let db=DatabaseHelper.getInstance()
////        if db == nil{return []}
//        // get data from db and store into array used by UITableView
//        let mainQuery = "SELECT name FROM sqlite_master WHERE type =\"table\""
//        let rsMain: FMResultSet? = db!.executeQuery(mainQuery, withArgumentsInArray: [])
//        while (rsMain?.next() == true) {
//            println(rsMain?.stringForColumn("name"))
//        }
//  }
  class func dump()
  {
//    for item in Apps.select(""){println("Apps "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in BusinessUnits.select(""){println("BusinessUnits "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in ContentSets.select(""){println("ContentSets "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Contents.select(""){println("Contents "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in CustomTrackContents.select(""){println("CustomTrackContents "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in CustomTracks.select(""){println("CustomTracks "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in DoctorCustomTracks.select(""){println("DoctorCustomTracks "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in DoctorPuzzlePieces.select(""){println("DoctorPuzzlePieces "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in DoctorSessionDetails.select(""){println("DoctorSessionDetails "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in DoctorSessions.select(""){println("DoctorSessions "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in DoctorTracks.select(""){println("DoctorTracks "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in DoctorVideos.select(""){println("DoctorVideos "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Doctors.select(""){println("Doctors "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Downloads.select(""){println("Downloads "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Employees.select(""){println("Employees "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Errors.select(""){println("Errors "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in LogEntries.select(""){println("LogEntries "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Notes.select(""){println("Notes "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in PuzzlePieces.select(""){println("PuzzlePieces "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Settings.select(""){println("Settings "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Tags.select(""){println("Tags "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Tracks.select(""){println("Tracks "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Users.select(""){println("Users "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Videos.select(""){println("Videos "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in ViewUpdates.select(""){println("ViewUpdates "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in Views.select(""){println("Views "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
//    for item in WordClouds.select(""){println("WordClouds "+item.commaSeparatedFieldsAndValues(includeId: true));println();}
  }
}